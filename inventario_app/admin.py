from django.contrib import admin
from .models import Colegios, Marca, Equipo, Inventario, Adicional, Monitor, Incidencia, Reporte, Extras, Despacho,User,Soporte,Laboratorio,Administrador, Condicion,Insumos,Insumoform
# Register your models here.

admin.site.register(Colegios)
admin.site.register(Marca)
admin.site.register(Equipo)
admin.site.register(Inventario)
admin.site.register(Adicional)
admin.site.register(Monitor)
admin.site.register(Incidencia)
admin.site.register(Reporte)
admin.site.register(Extras)
admin.site.register(Despacho)
admin.site.register(Condicion)
admin.site.register(User)
admin.site.register(Soporte)
admin.site.register(Laboratorio)
admin.site.register(Administrador)
admin.site.register(Insumos)
admin.site.register(Insumoform)