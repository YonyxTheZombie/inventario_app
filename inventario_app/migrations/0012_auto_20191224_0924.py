# Generated by Django 3.0 on 2019-12-24 12:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventario_app', '0011_despacho_extras'),
    ]

    operations = [
        migrations.AlterField(
            model_name='despacho',
            name='equipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventario_app.Adicional'),
        ),
    ]
