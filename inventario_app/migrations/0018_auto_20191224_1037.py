# Generated by Django 3.0 on 2019-12-24 13:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventario_app', '0017_auto_20191224_1036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='despacho',
            name='destino',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventario_app.Colegios'),
        ),
        migrations.AlterField(
            model_name='historicaldespacho',
            name='destino',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='inventario_app.Colegios'),
        ),
    ]
