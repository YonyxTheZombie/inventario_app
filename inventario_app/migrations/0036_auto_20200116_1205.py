# Generated by Django 2.2.7 on 2020-01-16 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventario_app', '0035_insumoform'),
    ]

    operations = [
        migrations.AlterField(
            model_name='insumoform',
            name='cantidad',
            field=models.FloatField(default=0),
        ),
    ]
