import sys
import math
import random
from io import BytesIO
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe
from model_utils.managers import InheritanceManager
from django.utils.timezone import utc
from simple_history.models import HistoricalRecords

# Create your models here.



class User(AbstractUser):
    is_administrador = models.BooleanField(default=False)
    is_soporte = models.BooleanField(default=False)
    is_laboratorio = models.BooleanField(default=False)


def colegios_images(instance, filename):
    return 'UserImages/user_{0}/{1}'.format(instance.user.id, filename)

def inventario_images(instance, filename):
    return 'UserImages/user_{0}/{1}'.format(instance.user.id, filename)

def monitor_images(instance, filename):
    return 'UserImages/user_{0}/{1}'.format(instance.user.id, filename)

class Colegios(models.Model):
    name = models.CharField(max_length=250)
    imagen = models.ImageField( null = True, upload_to='colegios_images',default=False, blank=True,verbose_name=('Imagen'), unique=True)

    def __str__(self):
        return self.name

class Insumos(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Marca(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Equipo(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Extras(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Condicion(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Administrador(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    colegio = models.ManyToManyField(Colegios, related_name='colegio_administrador')

    def __str__(self):
        return self.user.username

class Soporte(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    colegio = models.ManyToManyField(Colegios, related_name='colegio_soporte')
    
    def __str__(self):
        return self.user.username
        
class Laboratorio(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    colegio = models.ManyToManyField(Colegios, related_name='colegio_laboratorio')
    
    def __str__(self):
        return self.user.username

class Insumoform(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=('Usuario'))
    insumo = models.ForeignKey(Insumos, on_delete=models.CASCADE, verbose_name=('Insumo')) 
    colegio = models.ForeignKey(Colegios, on_delete=models.CASCADE)
    factura = models.CharField(max_length=250)
    cantidad = models.FloatField(default=0)
    fecha = models.DateField(auto_now_add=False,auto_now=False, verbose_name='Fecha de compra')
    proveedor = models.CharField(max_length=250)
    def __str__(self):
        return self.insumo.name


class Insumo_Egreso(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=('Usuario'))
    insumo = models.ForeignKey(Insumos, on_delete=models.CASCADE, verbose_name=('Insumo'))
    cantidad = models.FloatField(default=0)

    def __str__(self):
        return self.insumo.name
    
class Inventario(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=('Usuario'))
    codigo = models.CharField(max_length=250, verbose_name=('Codigo de equipo'), help_text='Ejemplo: PC001')
    colegio = models.ForeignKey(Colegios, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    modelo = models.CharField(max_length=250)
    serie = models.CharField(max_length=250)
    numero_doc = models.CharField(max_length=250, verbose_name=('N° de documento'), help_text='Puede ser el número de factura, boleta, etc',null=True, blank=True)
    Fecha = models.DateField(auto_now_add=False,auto_now=False, verbose_name='Fecha de compra')
    ubicacion = models.CharField(max_length=250)
    usuario = models.CharField(max_length=500, verbose_name=('Usuario Responsable'))
    valido = models.BooleanField(null= False, default=True, verbose_name=('validar'), help_text='Cuando esta activa esta opción el formulario estara válido.')
    imagen = models.ImageField(  upload_to='inventario_images', verbose_name=('Imagen'))
    condicion = models.ForeignKey(Condicion, on_delete=models.CASCADE ,verbose_name=('Condición'))

    history = HistoricalRecords()


    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def __str__(self):
        return str(self.id)

    def delete(self, *args, **kwargs):
        self.imagen.delete()
        super().delete(*args, **kwargs)

class Images(models.Model):
    inventario = models.ForeignKey(Inventario, on_delete=models.CASCADE)
    image = models.ImageField( upload_to='inventario_images', verbose_name=('Imagen'), blank=True, null=True)

    def __str__(self):
        return str(self.inventario.id)

class Adicional(models.Model):
    inventario = models.ForeignKey(Inventario, on_delete=models.CASCADE, related_name='inventarios')
    procesador = models.CharField(max_length=250)
    ram = models.CharField(max_length=250)
    sistema = models.CharField(max_length=250, help_text='Sistema operativo')
    disco = models.CharField(max_length=250)
    placa = models.CharField(max_length=250, help_text='Placa Madre')
    office = models.CharField(max_length=250, blank=True, null=True)
    history = HistoricalRecords()


    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def __str__(self):
        return str(self.inventario_id)

class Monitor(models.Model):
    inventario = models.ForeignKey(Inventario, on_delete=models.CASCADE, related_name='monitores')
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    modelo = models.CharField(max_length=250)
    serie = models.CharField(max_length=250)
    imagen = models.ImageField( null = True, upload_to='monitor_images', blank=True,verbose_name=('Imagen'))
    history = HistoricalRecords()

    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def __str__(self):
        return self.modelo
    
    def delete(self, *args, **kwargs):
        self.imagen.delete()
        super().delete(*args, **kwargs)

class Incidencia(models.Model):
    inventario = models.ForeignKey(Inventario, on_delete=models.CASCADE, related_name='incidencias')
    texto = models.TextField(max_length=5000)
    created = models.DateField(auto_now_add=True)
    history = HistoricalRecords()


    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def __str__(self):
        return self.texto


class Reporte(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    colegio = models.ForeignKey(Colegios, on_delete=models.CASCADE)
    texto = models.TextField(max_length=5000)
    created = models.DateField(auto_now_add=True)
    tiempo = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()


    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret

    def __str__(self):
        return self.texto

class ReporteNew(models.Model):
    reporte = models.ForeignKey(Reporte, on_delete=models.CASCADE)
    texto = models.TextField(max_length=5000)
    tiempo = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()


    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret
        
    def __str__(self):
        return self.texto


class Despacho(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name=('Usuario'))
    usuario = models.CharField(max_length=250, verbose_name=('Usuario'))
    area = models.CharField(max_length=250)
    origen = models.ForeignKey(Colegios, on_delete=models.CASCADE, related_name='origen')
    solicitado = models.CharField(max_length=250)
    fecha_entrega = models.DateTimeField(auto_now_add=False,auto_now=False, verbose_name='Fecha de entrega')
    fecha = models.DateField(auto_now_add=True)
    destino = models.ForeignKey(Colegios, on_delete=models.CASCADE, related_name='destino')
    equipo = models.ForeignKey(Inventario, on_delete=models.CASCADE)
    extras = models.ManyToManyField(Extras, related_name='extras')
    history = HistoricalRecords()

    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret
    
    def __str__(self):
        return str(self.id)

    
    