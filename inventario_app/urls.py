from django.urls import include, path
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from .views import inventario_app, administrador, soporte, laboratorio

urlpatterns = [
    path('', inventario_app.home, name='home'),

    path('administrador/', include(([
        path('', administrador.index, name='index'),
        path('despacho/<int:pk>/reporte_todos_excel/',administrador.ReporteTodosExcel.as_view(), name="reporte_todos_excel"),
        path('insumo/BCAM', administrador.insumo_list, name='BCAM'),
        path('insumo/index', administrador.insumo_index, name='insumo_index'),
        path('insumo/CSAB/', administrador.baluarte, name='CSAB'),
        path('insumo/BCM/', administrador.bc_maipu, name='BCM'),
        path('insumo/BCLF/',administrador.bc_farfana, name='BCLF'),
        path('insumo/CEB/',administrador.el_bosque, name='CEB'),
        path('insumo/CICV/',administrador.inmaculada_vitacura, name='CICV'),
        path('insumo/CLA/',administrador.los_alpes, name='CLA'),
        path('insumo/CMAN/',administrador.manantial, name='CMAN'),
        path('insumo/CSE/',administrador.sebastian_alcano, name='CSE'),
        path('insumo/CSFP/',administrador.san_francisco, name='CSFP'),
        path('insumo/CSM/',administrador.san_marcos, name='CSM'),
        path('insumo/IOM/',administrador.iom, name='IOM'),
        path('insumo/SSLA/',administrador.san_sebastia, name='SSLA'),
        path('insumo/add/', administrador.crear_insumo, name='insumo_form'),
        path('actividad/', administrador.actividad, name='actividad'),
        path('reportes/', administrador.reportes, name='reportes'),
        path('reportes/add/', administrador.crear_reporte, name='reporte_add'),
        path('reportes/<int:pk>/reportes_new/add/', administrador.crear_reporte_new, name='reporte_new_add'),
        path('reportes/<int:pk>', administrador.reportes_view, name='reportes_view'),
        path('inventario/<int:pk>/despacho/add/', administrador.crear_despacho, name='despacho'),
        path('despacho/', administrador.despacho, name='despacho_list'),
        path('despacho/<int:pk>', administrador.despacho_detail, name='despacho_detail'),
        path('usuarios/', administrador.usuarios, name='usuarios'),
        path('inventario/add/', administrador.crear_inventario, name='inventario_add'),
        path('inventario/<int:pk>/update', administrador.change_inventario, name='editar_inventario'),
        path('inventario/<int:pk>/delete/', administrador.InventarioDeleteView.as_view(), name='inventario_delete'),
        path('inventario/<int:pk>', administrador.descripcion, name='inventario_detalle'),
        path('inventario/<int:pk>/adicional/add/', administrador.crear_adicional, name='adicional_add'),
        path('inventario/<int:inventario_pk>/adicional/<int:adicional_pk>/', administrador.adicional_change, name='adicional_edit'),
        path('inventario/<int:pk>/monitor/add/', administrador.crear_monitor, name='monitor_add'),
        path('inventario/<int:inventario_pk>/monitor/<int:monitor_pk>/', administrador.monitor_change, name='monitor_edit'),
        path('inventario/<int:inventario_pk>/monitor/<int:monitor_pk>/delete/', administrador.MonitorDeleteView.as_view(), name='monitor_delete'),
        path('inventario/<int:pk>/incidencia/add/', administrador.crear_incidencia, name='incidencia_add'),


    ], 'inventario_app'), namespace='administrador')),

    path('soporte/', include(([
        path('', soporte.index, name='index'),
        path('reportes/', soporte.reportes, name='reportes'),
        path('reportes/add/', soporte.crear_reporte, name='reporte_add'),
        path('reportes/<int:pk>/reportes_new/add/', soporte.crear_reporte_new, name='reporte_new_add'),
        path('reportes/<int:pk>', soporte.reportes_view, name='reportes_view'),
        path('inventario/add/', soporte.CrearInventarioView.as_view(), name='inventario_add'),
        path('inventario/<int:pk>', soporte.descripcion, name='inventario_detalle'),
        path('inventario/<int:pk>/adicional/add/', soporte.crear_adicional, name='adicional_add'),
        path('inventario/<int:pk>/monitor/add/', soporte.crear_monitor, name='monitor_add'),
        path('inventario/<int:pk>/incidencia/add/', soporte.crear_incidencia, name='incidencia_add'),        
    ], 'inventario_app'), namespace='soporte')),
    
    path('laboratorio/', include(([
        path('', laboratorio.index, name='index'),
        path('reportes/', laboratorio.reportes, name='reportes'),
        path('reportes/add/', laboratorio.crear_reporte, name='reporte_add'),
        path('reportes/<int:pk>/reportes_new/add/', laboratorio.crear_reporte_new, name='reporte_new_add'),
        path('reportes/<int:pk>', laboratorio.reportes_view, name='reportes_view'),
        path('inventario/add/', laboratorio.CrearInventarioView.as_view(), name='inventario_add'),
        path('inventario/<int:pk>', laboratorio.descripcion, name='inventario_detalle'),
        path('inventario/<int:pk>/adicional/add/', laboratorio.crear_adicional, name='adicional_add'),
        path('inventario/<int:pk>/monitor/add/', laboratorio.crear_monitor, name='monitor_add'),
        path('inventario/<int:pk>/incidencia/add/', laboratorio.crear_incidencia, name='incidencia_add'),
    ], 'inventario_app'), namespace='laboratorio')),
    


]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)