import json
import traceback
from itertools import chain
from .__init__ import *
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE
from django.contrib.contenttypes.models import ContentType
from openpyxl import Workbook
from openpyxl.styles import Alignment, Border, PatternFill, Font, Side
from openpyxl.drawing.image import Image
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count, Sum, Min, Max, Func
from django.contrib.auth.models import User
from django.forms import inlineformset_factory, modelformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect, HttpRequest
from django.views.generic import (TemplateView,CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from ..decorators import administrador_required
from datetime import date, datetime                                  
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from inventario_app.forms import AdministradorSignUpForm, InsumoForm,CrearInventarioForm,EditarInventarioForm ,CrearCaracteristicasForm, CrearMonitorForm, IncidenciaForm, ReporteForm, ReporteNewForm,DespachoForm
from inventario_app.models import Inventario, Marca, User, Equipo, Administrador, Colegios, Adicional, Monitor, Incidencia, Reporte, ReporteNew, Despacho, Images, Insumoform, Insumos

# Create your views here.
class ReporteTodosExcel(TemplateView):
     
    #Usamos el método get para generar el archivo excel 
    def get(self, request, pk,*args, **kwargs):
        #Obtenemos todas las personas de nuestra base de datos
        personas = Despacho.objects.filter(pk=pk)
        #Creamos el libro de trabajo
        wb = Workbook()
        #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
        ws = wb.active
        llenar = PatternFill(start_color = 'FFFFFF', end_color='FFFFFF', fill_type="solid")
        borde = Border(left = Side(border_style = "none"), right= Side(border_style= "none"),
                                top= Side(border_style = "none"), bottom= Side(border_style= "none"))
        thin = Border(bottom=Side(border_style='medium'))
        medium = Border(left = Side(border_style = "medium"), right= Side(border_style= "medium"),
                                top= Side(border_style = "medium"), bottom= Side(border_style= "medium"))
        mediumii = Border(left = Side(border_style = "medium"), right= Side(border_style= "medium"),
                                 bottom= Side(border_style= "medium"))
        lado = Border(left = Side(border_style = "medium"), right= Side(border_style= "medium"))
        inferior = Border( bottom= Side(border_style= "medium"))

        img = Image('static/img/logo_2.jpg')
        #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
        ws['C5'] = 'FICHA DE ENTREGA DE EQUIPAMIENTO'
        ws['C5'].font = Font(name='Calibri', size=24, bold=True)
        ws['C5'].alignment = Alignment(indent=2 )

        #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
        ws.merge_cells('C5:H5')
        ws.merge_cells('G6:H6')
        ws.add_image(img, 'A1')

        ws['A1'].border = borde
        ws['A2'].border = borde
        ws['A3'].border = borde
        ws['A4'].border = borde
        ws['A5'].border = borde
        ws['A6'].border = borde
        ws['A7'].border = borde
        ws['A8'].border = borde
        ws['A9'].border = borde
        ws['A10'].border = borde
        ws['A11'].border = medium
        ws['A12'].border = lado
        ws['A13'].border = mediumii
        ws['A14'].border = borde
        ws['A15'].border = borde
        ws['A16'].border = medium
        ws['A17'].border = medium
        ws['A18'].border = borde
        ws['A19'].border = borde
        ws['A20'].border = borde
        ws['A21'].border = borde
        ws['A22'].border = borde
        ws['A23'].border = borde
        ws['A24'].border = borde
        ws['A25'].border = borde
        ws['A26'].border = borde
        ws['A27'].border = borde
        ws['A28'].border = borde
        ws['A29'].border = borde
        ws['A30'].border = borde
        ws['A31'].border = borde
        ws['A32'].border = borde
        ws['A33'].border = borde
        ws['A34'].border = borde
        ws['A35'].border = borde
        ws['A36'].border = borde
        ws['A1'].fill = llenar
        ws['A2'].fill = llenar
        ws['A3'].fill = llenar
        ws['A4'].fill = llenar
        ws['A5'].fill = llenar
        ws['A6'].fill = llenar
        ws['A7'].fill = llenar
        ws['A8'].fill = llenar
        ws['A9'].fill = llenar
        ws['A10'].fill = llenar
        ws['A11'].fill = llenar
        ws['A12'].fill = llenar
        ws['A13'].fill = llenar
        ws['A14'].fill = llenar
        ws['A15'].fill = llenar
        ws['A16'].fill = llenar
        ws['A17'].fill = llenar
        ws['A18'].fill = llenar
        ws['A19'].fill = llenar
        ws['A20'].fill = llenar
        ws['A21'].fill = llenar
        ws['A22'].fill = llenar
        ws['A23'].fill = llenar
        ws['A24'].fill = llenar
        ws['A25'].fill = llenar
        ws['A26'].fill = llenar
        ws['A27'].fill = llenar
        ws['A28'].fill = llenar
        ws['A29'].fill = llenar
        ws['A30'].fill = llenar
        ws['A31'].fill = llenar
        ws['A32'].fill = llenar
        ws['A33'].fill = llenar
        ws['A34'].fill = llenar
        ws['A35'].fill = llenar
        ws['A36'].fill = llenar

        ws['B1'].border = borde
        ws['B2'].border = borde
        ws['B3'].border = borde
        ws['B4'].border = borde
        ws['B5'].border = borde
        ws['B6'].border = borde
        ws['B7'].border = borde
        ws['B8'].border = borde
        ws['B9'].border = borde
        ws['B10'].border = borde
        ws['B11'].border = borde
        ws['B12'].border = lado
        ws['B13'].border = mediumii
        ws['B14'].border = borde
        ws['B15'].border = borde
        ws['B16'].border = medium
        ws['B17'].border = medium
        ws['B18'].border = borde
        ws['B19'].border = borde
        ws['B20'].border = borde
        ws['B21'].border = borde
        ws['B22'].border = borde
        ws['B23'].border = borde
        ws['B24'].border = borde
        ws['B25'].border = borde
        ws['B26'].border = borde
        ws['B27'].border = borde
        ws['B28'].border = borde
        ws['B29'].border = borde
        ws['B30'].border = borde
        ws['B31'].border = borde
        ws['B32'].border = borde
        ws['B33'].border = borde
        ws['B34'].border = borde
        ws['B35'].border = borde
        ws['B36'].border = borde
        ws['B1'].fill = llenar
        ws['B2'].fill = llenar
        ws['B3'].fill = llenar
        ws['B4'].fill = llenar
        ws['B5'].fill = llenar
        ws['B6'].fill = llenar
        ws['B7'].fill = llenar
        ws['B8'].fill = llenar
        ws['B9'].fill = llenar
        ws['B10'].fill = llenar
        ws['B11'].fill = llenar
        ws['B12'].fill = llenar
        ws['B13'].fill = llenar
        ws['B14'].fill = llenar
        ws['B15'].fill = llenar
        ws['B16'].fill = llenar
        ws['B17'].fill = llenar
        ws['B18'].fill = llenar
        ws['B19'].fill = llenar
        ws['B20'].fill = llenar
        ws['B21'].fill = llenar
        ws['B22'].fill = llenar
        ws['B23'].fill = llenar
        ws['B24'].fill = llenar
        ws['B25'].fill = llenar
        ws['B26'].fill = llenar
        ws['B27'].fill = llenar
        ws['B28'].fill = llenar
        ws['B29'].fill = llenar
        ws['B30'].fill = llenar
        ws['B31'].fill = llenar
        ws['B32'].fill = llenar
        ws['B33'].fill = llenar
        ws['B34'].fill = llenar
        ws['B35'].fill = llenar
        ws['B36'].fill = llenar

        ws['C2'].border = borde
        ws['C1'].border = borde
        ws['C3'].border = borde
        ws['C4'].border = borde
        ws['C5'].border = borde
        ws['C6'].border = thin
        ws['C7'].border = thin
        ws['C8'].border = thin
        ws['C9'].border = borde
        ws['C10'].border = borde
        ws['C11'].border = medium
        ws['C12'].border = lado
        ws['C13'].border = mediumii
        ws['C14'].border = borde
        ws['C15'].border = borde
        ws['C16'].border = medium
        ws['C17'].border = medium
        ws['C18'].border = borde
        ws['C19'].border = borde
        ws['C20'].border = borde
        ws['C21'].border = borde
        ws['C22'].border = borde
        ws['C23'].border = borde
        ws['C24'].border = borde
        ws['C25'].border = borde
        ws['C26'].border = borde
        ws['C27'].border = borde
        ws['C28'].border = borde
        ws['C29'].border = borde
        ws['C30'].border = borde
        ws['C31'].border = borde
        ws['C32'].border = borde
        ws['C33'].border = borde
        ws['C34'].border = borde
        ws['C35'].border = borde
        ws['C36'].border = borde
        ws['C1'].fill = llenar
        ws['C2'].fill = llenar
        ws['C3'].fill = llenar
        ws['C4'].fill = llenar
        ws['C5'].fill = llenar
        ws['C7'].fill = llenar
        ws['C8'].fill = llenar
        ws['C9'].fill = llenar
        ws['C10'].fill = llenar
        ws['C11'].fill = llenar
        ws['C12'].fill = llenar
        ws['C13'].fill = llenar
        ws['C14'].fill = llenar
        ws['C15'].fill = llenar
        ws['C16'].fill = llenar
        ws['C17'].fill = llenar
        ws['C18'].fill = llenar
        ws['C19'].fill = llenar
        ws['C20'].fill = llenar
        ws['C21'].fill = llenar
        ws['C22'].fill = llenar
        ws['C23'].fill = llenar
        ws['C24'].fill = llenar
        ws['C25'].fill = llenar
        ws['C26'].fill = llenar
        ws['C27'].fill = llenar
        ws['C28'].fill = llenar
        ws['C29'].fill = llenar
        ws['C30'].fill = llenar
        ws['C31'].fill = llenar
        ws['C32'].fill = llenar
        ws['C33'].fill = llenar
        ws['C34'].fill = llenar
        ws['C35'].fill = llenar
        ws['C36'].fill = llenar

        ws['D1'].border = borde
        ws['D2'].border = borde
        ws['D3'].border = borde
        ws['D4'].border = borde
        ws['D5'].border = borde
        ws['D6'].border = thin
        ws['D7'].border = thin
        ws['D8'].border = thin
        ws['D9'].border = borde
        ws['D10'].border = borde
        ws['D11'].border = medium
        ws['D12'].border = borde
        ws['D13'].border = mediumii
        ws['D14'].border = borde
        ws['D15'].border = borde
        ws['D16'].border = medium
        ws['D17'].border = medium
        ws['D18'].border = borde
        ws['D19'].border = borde
        ws['D20'].border = borde
        ws['D21'].border = borde
        ws['D22'].border = borde
        ws['D23'].border = borde
        ws['D24'].border = borde
        ws['D25'].border = borde
        ws['D26'].border = borde
        ws['D27'].border = borde
        ws['D28'].border = borde
        ws['D29'].border = borde
        ws['D30'].border = borde
        ws['D31'].border = borde
        ws['D32'].border = borde
        ws['D33'].border = borde
        ws['D34'].border = borde
        ws['D35'].border = borde
        ws['D36'].border = borde
        ws['D1'].fill = llenar
        ws['D2'].fill = llenar
        ws['D3'].fill = llenar
        ws['D4'].fill = llenar
        ws['D5'].fill = llenar
        ws['D6'].fill = llenar
        ws['D7'].fill = llenar
        ws['D8'].fill = llenar
        ws['D9'].fill = llenar
        ws['D10'].fill = llenar
        ws['D11'].fill = llenar
        ws['D12'].fill = llenar
        ws['D13'].fill = llenar
        ws['D14'].fill = llenar
        ws['D15'].fill = llenar
        ws['D16'].fill = llenar
        ws['D17'].fill = llenar
        ws['D18'].fill = llenar
        ws['D19'].fill = llenar
        ws['D20'].fill = llenar
        ws['D21'].fill = llenar
        ws['D22'].fill = llenar
        ws['D23'].fill = llenar
        ws['D24'].fill = llenar
        ws['D25'].fill = llenar
        ws['D26'].fill = llenar
        ws['D27'].fill = llenar
        ws['D28'].fill = llenar
        ws['D29'].fill = llenar
        ws['D30'].fill = llenar
        ws['D31'].fill = llenar
        ws['D32'].fill = llenar
        ws['D33'].fill = llenar
        ws['D34'].fill = llenar
        ws['D35'].fill = llenar
        ws['D36'].fill = llenar

        ws['E1'].border = borde
        ws['E2'].border = borde
        ws['E3'].border = borde
        ws['E4'].border = borde
        ws['E5'].border = borde
        ws['E6'].border = thin
        ws['E7'].border = thin
        ws['E8'].border = thin
        ws['E9'].border = borde
        ws['E10'].border = borde
        ws['E11'].border = mediumii
        ws['E12'].border = lado
        ws['E13'].border = medium
        ws['E14'].border = borde
        ws['E15'].border = borde
        ws['E16'].border = medium
        ws['E17'].border = medium
        ws['E18'].border = borde
        ws['E19'].border = borde
        ws['E20'].border = borde
        ws['E21'].border = borde
        ws['E22'].border = borde
        ws['E23'].border = borde
        ws['E24'].border = borde
        ws['E25'].border = borde
        ws['E26'].border = borde 
        ws['E27'].border = borde       
        ws['E28'].border = borde
        ws['E29'].border = borde
        ws['E30'].border = borde
        ws['E31'].border = borde
        ws['E32'].border = borde
        ws['E33'].border = borde
        ws['E34'].border = borde
        ws['E35'].border = borde
        ws['E36'].border = borde
        ws['E1'].fill = llenar
        ws['E2'].fill = llenar
        ws['E3'].fill = llenar
        ws['E4'].fill = llenar
        ws['E5'].fill = llenar
        ws['E6'].fill = llenar
        ws['E7'].fill = llenar
        ws['E8'].fill = llenar
        ws['E9'].fill = llenar
        ws['E10'].fill = llenar
        ws['E11'].fill = llenar
        ws['E12'].fill = llenar
        ws['E13'].fill = llenar
        ws['E14'].fill = llenar
        ws['E15'].fill = llenar
        ws['E16'].fill = llenar
        ws['E17'].fill = llenar
        ws['E18'].fill = llenar
        ws['E19'].fill = llenar
        ws['E20'].fill = llenar
        ws['E21'].fill = llenar
        ws['E22'].fill = llenar
        ws['E23'].fill = llenar
        ws['E24'].fill = llenar
        ws['E25'].fill = llenar
        ws['E26'].fill = llenar
        ws['E27'].fill = llenar
        ws['E28'].fill = llenar
        ws['E29'].fill = llenar
        ws['E30'].fill = llenar
        ws['E31'].fill = llenar
        ws['E32'].fill = llenar
        ws['E33'].fill = llenar
        ws['E34'].fill = llenar
        ws['E35'].fill = llenar
        ws['E36'].fill = llenar

        ws['F1'].border = borde
        ws['F2'].border = borde
        ws['F3'].border = borde
        ws['F4'].border = borde
        ws['F5'].border = borde
        ws['F6'].border = borde
        ws['F7'].border = borde
        ws['F8'].border = borde
        ws['F9'].border = borde
        ws['F10'].border = borde
        ws['F11'].border = medium
        ws['F12'].border = lado
        ws['F13'].border = mediumii
        ws['F14'].border = borde
        ws['F15'].border = borde
        ws['F16'].border = medium
        ws['F17'].border = medium
        ws['F18'].border = borde
        ws['F19'].border = borde
        ws['F20'].border = borde
        ws['F21'].border = borde
        ws['F22'].border = borde
        ws['F23'].border = borde
        ws['F24'].border = borde
        ws['F25'].border = borde
        ws['F26'].border = inferior
        ws['F27'].border = borde
        ws['F28'].border = borde
        ws['F29'].border = borde
        ws['F30'].border = borde
        ws['F31'].border = borde
        ws['F32'].border = borde
        ws['F33'].border = borde
        ws['F34'].border = borde
        ws['F35'].border = borde
        ws['F36'].border = borde
        ws['F1'].fill = llenar
        ws['F2'].fill = llenar
        ws['F3'].fill = llenar
        ws['F4'].fill = llenar
        ws['F5'].fill = llenar
        ws['F6'].fill = llenar
        ws['F7'].fill = llenar
        ws['F8'].fill = llenar
        ws['F9'].fill = llenar
        ws['F10'].fill = llenar
        ws['F11'].fill = llenar
        ws['F12'].fill = llenar
        ws['F13'].fill = llenar
        ws['F14'].fill = llenar
        ws['F15'].fill = llenar
        ws['F16'].fill = llenar
        ws['F17'].fill = llenar
        ws['F18'].fill = llenar
        ws['F19'].fill = llenar
        ws['F20'].fill = llenar
        ws['F21'].fill = llenar
        ws['F22'].fill = llenar
        ws['F23'].fill = llenar
        ws['F24'].fill = llenar
        ws['F25'].fill = llenar
        ws['F26'].fill = llenar
        ws['F27'].fill = llenar
        ws['F28'].fill = llenar
        ws['F29'].fill = llenar
        ws['F30'].fill = llenar
        ws['F31'].fill = llenar
        ws['F32'].fill = llenar
        ws['F33'].fill = llenar
        ws['F34'].fill = llenar
        ws['F35'].fill = llenar
        ws['F36'].fill = llenar

        ws['G1'].border = borde
        ws['G2'].border = borde
        ws['G3'].border = borde
        ws['G4'].border = borde
        ws['G5'].border = borde
        ws['G6'].border = borde
        ws['G7'].border = borde
        ws['G8'].border = borde
        ws['G9'].border = borde
        ws['G10'].border = borde
        ws['G11'].border = medium
        ws['G12'].border = borde
        ws['G13'].border = mediumii
        ws['G14'].border = borde
        ws['G15'].border = borde
        ws['G16'].border = medium
        ws['G17'].border = medium
        ws['G18'].border = borde
        ws['G19'].border = borde
        ws['G20'].border = borde
        ws['G21'].border = borde
        ws['G22'].border = borde
        ws['G23'].border = borde
        ws['G24'].border = borde
        ws['G25'].border = borde
        ws['G26'].border = borde
        ws['G27'].border = borde
        ws['G28'].border = borde
        ws['G29'].border = borde
        ws['G30'].border = borde
        ws['G31'].border = borde
        ws['G32'].border = borde
        ws['G33'].border = borde
        ws['G34'].border = borde
        ws['G35'].border = borde
        ws['G36'].border = borde
        ws['G1'].fill = llenar
        ws['G2'].fill = llenar
        ws['G3'].fill = llenar
        ws['G4'].fill = llenar
        ws['G5'].fill = llenar
        ws['G6'].fill = llenar
        ws['G7'].fill = llenar
        ws['G8'].fill = llenar
        ws['G9'].fill = llenar
        ws['G10'].fill = llenar
        ws['G11'].fill = llenar
        ws['G12'].fill = llenar
        ws['G13'].fill = llenar
        ws['G14'].fill = llenar
        ws['G15'].fill = llenar
        ws['G16'].fill = llenar
        ws['G17'].fill = llenar
        ws['G18'].fill = llenar
        ws['G19'].fill = llenar
        ws['G20'].fill = llenar
        ws['G21'].fill = llenar
        ws['G22'].fill = llenar
        ws['G23'].fill = llenar
        ws['G24'].fill = llenar
        ws['G25'].fill = llenar
        ws['G26'].fill = llenar
        ws['G27'].fill = llenar
        ws['G28'].fill = llenar
        ws['G29'].fill = llenar
        ws['G30'].fill = llenar
        ws['G31'].fill = llenar
        ws['G32'].fill = llenar
        ws['G33'].fill = llenar
        ws['G34'].fill = llenar
        ws['G35'].fill = llenar
        ws['G36'].fill = llenar

        ws['H1'].border = borde
        ws['H2'].border = borde
        ws['H3'].border = borde
        ws['H4'].border = borde
        ws['H5'].border = borde
        ws['H6'].border = borde
        ws['H7'].border = borde
        ws['H8'].border = borde
        ws['H9'].border = borde
        ws['H10'].border = borde
        ws['H11'].border = medium
        ws['H12'].border = lado
        ws['H13'].border = mediumii
        ws['H14'].border = borde
        ws['H15'].border = borde
        ws['H16'].border = medium
        ws['H17'].border = medium
        ws['H18'].border = borde
        ws['H19'].border = borde
        ws['H20'].border = borde
        ws['H21'].border = borde
        ws['H22'].border = borde
        ws['H23'].border = borde
        ws['H24'].border = borde
        ws['H25'].border = borde
        ws['H26'].border = borde
        ws['H27'].border = borde
        ws['H28'].border = borde
        ws['H29'].border = borde
        ws['H30'].border = borde
        ws['H31'].border = borde
        ws['H32'].border = borde
        ws['H33'].border = borde
        ws['H34'].border = borde
        ws['H35'].border = borde
        ws['H36'].border = borde
        ws['H1'].fill = llenar
        ws['H2'].fill = llenar
        ws['H3'].fill = llenar
        ws['H4'].fill = llenar
        ws['H5'].fill = llenar
        ws['H6'].fill = llenar
        ws['H7'].fill = llenar
        ws['H8'].fill = llenar
        ws['H9'].fill = llenar
        ws['H10'].fill = llenar
        ws['H11'].fill = llenar
        ws['H12'].fill = llenar
        ws['H13'].fill = llenar
        ws['H14'].fill = llenar
        ws['H15'].fill = llenar
        ws['H16'].fill = llenar
        ws['H17'].fill = llenar
        ws['H18'].fill = llenar
        ws['H19'].fill = llenar
        ws['H20'].fill = llenar
        ws['H21'].fill = llenar
        ws['H22'].fill = llenar
        ws['H23'].fill = llenar
        ws['H24'].fill = llenar
        ws['H25'].fill = llenar
        ws['H26'].fill = llenar
        ws['H27'].fill = llenar
        ws['H28'].fill = llenar
        ws['H29'].fill = llenar
        ws['H30'].fill = llenar
        ws['H31'].fill = llenar
        ws['H32'].fill = llenar
        ws['H33'].fill = llenar
        ws['H34'].fill = llenar
        ws['H35'].fill = llenar
        ws['H36'].fill = llenar

        ws['I1'].border = borde
        ws['I2'].border = thin
        ws['I3'].border = borde
        ws['I4'].border = borde
        ws['I5'].border = borde
        ws['I6'].border = thin
        ws['I7'].border = thin
        ws['I8'].border = thin
        ws['I9'].border = borde
        ws['I10'].border = borde
        ws['I11'].border = medium
        ws['I12'].border = lado
        ws['I13'].border = mediumii
        ws['I14'].border = borde
        ws['I15'].border = borde
        ws['I16'].border = medium
        ws['I17'].border = medium
        ws['I18'].border = borde
        ws['I19'].border = borde
        ws['I20'].border = borde
        ws['I21'].border = borde
        ws['I22'].border = borde
        ws['I23'].border = borde
        ws['I24'].border = borde
        ws['I25'].border = borde
        ws['I26'].border = borde
        ws['I27'].border = borde
        ws['I28'].border = borde
        ws['I29'].border = borde
        ws['I30'].border = borde
        ws['I31'].border = borde
        ws['I32'].border = borde
        ws['I33'].border = borde
        ws['I34'].border = borde
        ws['I35'].border = borde
        ws['I36'].border = borde
        ws['I1'].fill = llenar
        ws['I2'].fill = llenar
        ws['I3'].fill = llenar
        ws['I4'].fill = llenar
        ws['I5'].fill = llenar
        ws['I6'].fill = llenar
        ws['I7'].fill = llenar
        ws['I8'].fill = llenar
        ws['I9'].fill = llenar
        ws['I10'].fill = llenar
        ws['I11'].fill = llenar
        ws['I12'].fill = llenar
        ws['I13'].fill = llenar
        ws['I14'].fill = llenar
        ws['I15'].fill = llenar
        ws['I16'].fill = llenar
        ws['I17'].fill = llenar
        ws['I18'].fill = llenar
        ws['I19'].fill = llenar
        ws['I20'].fill = llenar
        ws['I21'].fill = llenar
        ws['I22'].fill = llenar
        ws['I23'].fill = llenar
        ws['I24'].fill = llenar
        ws['I25'].fill = llenar
        ws['I26'].fill = llenar
        ws['I27'].fill = llenar
        ws['I28'].fill = llenar
        ws['I29'].fill = llenar
        ws['I30'].fill = llenar
        ws['I31'].fill = llenar
        ws['I32'].fill = llenar
        ws['I33'].fill = llenar
        ws['I34'].fill = llenar
        ws['I35'].fill = llenar
        ws['I36'].fill = llenar

        ws['J1'].border = borde
        ws['J2'].border = borde
        ws['J3'].border = borde
        ws['J4'].border = borde
        ws['J5'].border = borde
        ws['J6'].border = thin
        ws['J7'].border = thin
        ws['J8'].border = thin
        ws['J9'].border = borde
        ws['J10'].border = borde
        ws['J11'].border = medium
        ws['J12'].border = lado
        ws['J13'].border = mediumii
        ws['J14'].border = borde
        ws['J15'].border = borde
        ws['J16'].border = borde
        ws['J17'].border = borde
        ws['J18'].border = borde
        ws['J19'].border = borde
        ws['J20'].border = borde
        ws['J21'].border = borde
        ws['J22'].border = borde
        ws['J23'].border = borde
        ws['J24'].border = borde
        ws['J25'].border = borde
        ws['J26'].border = borde
        ws['J27'].border = borde
        ws['J28'].border = borde
        ws['J29'].border = borde
        ws['J30'].border = borde
        ws['J31'].border = borde
        ws['J32'].border = borde
        ws['J33'].border = borde
        ws['J34'].border = borde
        ws['J35'].border = borde
        ws['J36'].border = borde

        ws['J1'].fill = llenar
        ws['J2'].fill = llenar
        ws['J3'].fill = llenar
        ws['J4'].fill = llenar
        ws['J5'].fill = llenar
        ws['J6'].fill = llenar
        ws['J7'].fill = llenar
        ws['J8'].fill = llenar
        ws['J9'].fill = llenar
        ws['J10'].fill = llenar
        ws['J11'].fill = llenar
        ws['J12'].fill = llenar
        ws['J13'].fill = llenar
        ws['J14'].fill = llenar
        ws['J15'].fill = llenar
        ws['J16'].fill = llenar
        ws['J17'].fill = llenar
        ws['J18'].fill = llenar
        ws['J19'].fill = llenar
        ws['J20'].fill = llenar
        ws['J21'].fill = llenar
        ws['J22'].fill = llenar
        ws['J23'].fill = llenar
        ws['J24'].fill = llenar
        ws['J25'].fill = llenar
        ws['J26'].fill = llenar
        ws['J27'].fill = llenar
        ws['J28'].fill = llenar
        ws['J29'].fill = llenar
        ws['J30'].fill = llenar
        ws['J31'].fill = llenar
        ws['J32'].fill = llenar
        ws['J33'].fill = llenar
        ws['J34'].fill = llenar
        ws['J35'].fill = llenar
        ws['J36'].fill = llenar

        ws['K1'].fill = llenar
        ws['K2'].fill = llenar
        ws['K3'].fill = llenar
        ws['K4'].fill = llenar
        ws['K5'].fill = llenar
        ws['K6'].fill = llenar
        ws['K7'].fill = llenar
        ws['K8'].fill = llenar
        ws['K9'].fill = llenar
        ws['K10'].fill = llenar
        ws['K11'].fill = llenar
        ws['K12'].fill = llenar
        ws['K13'].fill = llenar
        ws['K14'].fill = llenar
        ws['K15'].fill = llenar
        ws['K16'].fill = llenar
        ws['K17'].fill = llenar
        ws['K18'].fill = llenar
        ws['K19'].fill = llenar
        ws['K20'].fill = llenar
        ws['K21'].fill = llenar
        ws['K22'].fill = llenar
        ws['K23'].fill = llenar
        ws['K24'].fill = llenar
        ws['K25'].fill = llenar
        ws['K26'].fill = llenar
        ws['K27'].fill = llenar
        ws['K28'].fill = llenar
        ws['K29'].fill = llenar
        ws['K30'].fill = llenar
        ws['K31'].fill = llenar
        ws['K32'].fill = llenar
        ws['K33'].fill = llenar
        ws['K34'].fill = llenar
        ws['K35'].fill = llenar
        ws['K36'].fill = llenar


        ws.column_dimensions['A'].width = float(13.29)
        ws.column_dimensions['B'].width = float(2.71)
        ws.column_dimensions['C'].width = float(21.71)
        ws.column_dimensions['D'].width = float(1.43)
        ws.column_dimensions['E'].width = float(20.71)
        ws.column_dimensions['F'].width = float(16.71)
        ws.column_dimensions['G'].width = float(2.43)
        ws.column_dimensions['H'].width = float(21.14)
        ws.column_dimensions['I'].width = float(19.14)
        ws.column_dimensions['J'].width = float(19.14)


        #Creamos los encabezados 
        ws['A6'] = 'USUARIO:'
        ws['A7'] = 'ÁREA:'
        ws['A8'] = 'ORIGEN:'  
        ws['G6'] = 'SOLICITADO:' 
        ws['G7'] = 'FECHA ENTREGA:'
        ws['G8'] = 'DESTINO:'
        ws['H2'] = 'Fecha:'

        ws['A10'] = 'COMPUTADORES/NOTEBOOKS'
        ws['A11'] = 'TIPO'
        ws['C11'] = 'MARCA'
        ws['D11'] = 'CÓDIGO'
        ws['F11'] = 'MODELO'
        ws['G11'] = 'SERIE'
        ws['I11'] = 'CONDICIÓN'
        ws['J11'] = 'FACTURA'
        ws['F27'] = 'FIRMA RECIBIDO CONFORME'
        #Unión de celdas
        ws.merge_cells('A11:B11')
        ws.merge_cells('D11:E11')
        ws.merge_cells('G11:H11')
        ws.merge_cells('G13:H13')
        ws.merge_cells('A13:B13')
        ws.merge_cells('D13:E13')
        ws.merge_cells('G12:H12')
        ws.merge_cells('A12:B12')
        ws.merge_cells('D12:E12')
        ws.merge_cells('A16:B16')
        ws.merge_cells('D16:E16')
        ws.merge_cells('G16:H16')
        ws.merge_cells('A17:B17')
        ws.merge_cells('D17:E17')
        ws.merge_cells('G17:H17')
        ws.merge_cells('F26:I26')
        ws.merge_cells('F27:I27')

        ws['A15'] = 'CARACTERÍSTICAS'
        
        ws['A16'] = 'PROCESADOR'
        ws['C16'] = 'RAM'
        ws['D16'] = 'SISTEMA'
        ws['F16'] = 'DISCO'
        ws['G16'] = 'PLACA'
        ws['I16'] = 'OFFICE'
        




        ws['A10'].font = Font(name='Calibri', size=11, bold=True)
        ws['A11'].font = Font(name='Calibri', size=11, bold=True)
        ws['C11'].font = Font(name='Calibri', size=11, bold=True)
        ws['D11'].font = Font(name='Calibri', size=11, bold=True)
        ws['F11'].font = Font(name='Calibri', size=11, bold=True)
        ws['G11'].font = Font(name='Calibri', size=11, bold=True)
        ws['I11'].font = Font(name='Calibri', size=11, bold=True)
        ws['J11'].font = Font(name='Calibri', size=11, bold=True)
        ws['A11'].alignment = Alignment(horizontal='center')
        ws['C11'].alignment = Alignment(horizontal='center')
        ws['D11'].alignment = Alignment(horizontal='center')
        ws['F11'].alignment = Alignment(horizontal='center')
        ws['G11'].alignment = Alignment(horizontal='center')
        ws['F11'].alignment = Alignment(horizontal='center')
        ws['I11'].alignment = Alignment(horizontal='center')
        ws['J11'].alignment = Alignment(horizontal='center')
        ws['F27'].alignment = Alignment(horizontal='center')



        ws['A12'].alignment = Alignment(horizontal='center', vertical='center')
        ws['C12'].alignment = Alignment(horizontal='center', vertical='center')
        ws['D12'].alignment = Alignment(horizontal='center', vertical='center')
        ws['F12'].alignment = Alignment(horizontal='center', vertical='center')
        ws['G12'].alignment = Alignment(horizontal='center', vertical='center')
        ws['F12'].alignment = Alignment(horizontal='center', wrap_text=True, vertical='center')
        ws['I12'].alignment = Alignment(horizontal='center', vertical='center')
        ws['J12'].alignment = Alignment(horizontal='center', vertical='center')

        ws['A15'].font = Font(name='Calibri', size=11, bold=True)
        ws['A16'].font = Font(name='Calibri', size=11, bold=True)
        ws['C16'].font = Font(name='Calibri', size=11, bold=True)
        ws['D16'].font = Font(name='Calibri', size=11, bold=True)
        ws['F16'].font = Font(name='Calibri', size=11, bold=True)
        ws['G16'].font = Font(name='Calibri', size=11, bold=True)
        ws['I16'].font = Font(name='Calibri', size=11, bold=True)
        ws['J16'].font = Font(name='Calibri', size=11, bold=True)

        ws['A19'].font = Font(name='Calibri', size=11, bold=True)
        ws['A20'].font = Font(name='Calibri', size=11, bold=True)
        ws['A21'].font = Font(name='Calibri', size=11, bold=True)
        ws['A22'].font = Font(name='Calibri', size=11, bold=True)
        ws['A23'].font = Font(name='Calibri', size=11, bold=True)
        ws['A24'].font = Font(name='Calibri', size=11, bold=True)

        ws['A16'].alignment = Alignment(horizontal='center')
        ws['C16'].alignment = Alignment(horizontal='center')
        ws['D16'].alignment = Alignment(horizontal='center')
        ws['F16'].alignment = Alignment(horizontal='center')
        ws['G16'].alignment = Alignment(horizontal='center')
        ws['F16'].alignment = Alignment(horizontal='center')
        ws['I16'].alignment = Alignment(horizontal='center')
        ws['J16'].alignment = Alignment(horizontal='center')
        ws['A17'].alignment = Alignment(horizontal='center')
        ws['C17'].alignment = Alignment(horizontal='center')
        ws['D17'].alignment = Alignment(horizontal='center')
        ws['F17'].alignment = Alignment(horizontal='center')
        ws['G17'].alignment = Alignment(horizontal='center')
        ws['F17'].alignment = Alignment(horizontal='center')
        ws['I17'].alignment = Alignment(horizontal='center')
        ws['J17'].alignment = Alignment(horizontal='center')

        #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
      
        

        for persona in personas:
            fecha = persona.fecha_entrega.strftime("%d-%m-%Y")
            fecha_ahora = persona.fecha.strftime("%d-%m-%Y")
            ws.cell(row=2, column=9). value = fecha_ahora
            ws.cell(row=6,column=3).value = persona.usuario.upper()
            ws.cell(row=7,column=3).value = persona.area.upper()
            ws.cell(row=8,column=3).value = persona.origen.name.upper()
            ws.cell(row=6,column=9).value = persona.solicitado.upper()
            ws.cell(row=7,column=9).value = fecha
            ws.cell(row=8,column=9).value = persona.destino.name.upper()
            ws.cell(row=12,column=1).value = persona.equipo.equipo.name.upper()
            ws.cell(row=12,column=3).value = persona.equipo.marca.name.upper()
            ws.cell(row=12,column=4).value = persona.equipo.codigo.upper()
            ws.cell(row=12,column=6).value = persona.equipo.modelo.upper()
            ws.cell(row=12,column=7).value = persona.equipo.serie.upper()
            ws.cell(row=12,column=9).value = persona.equipo.condicion.name.upper()
            ws.cell(row=12,column=10).value = persona.equipo.numero_doc

            ws['C6'].font = Font(name='Calibri', size=11, bold=True,)
            ws['C7'].font = Font(name='Calibri', size=11, bold=True)
            ws['C8'].font = Font(name='Calibri', size=11, bold=True)
            ws['I6'].font = Font(name='Calibri', size=11, bold=True)
            ws['I7'].font = Font(name='Calibri', size=11, bold=True)
            ws['I8'].font = Font(name='Calibri', size=11, bold=True)
            for inventarios in persona.equipo.inventarios.all():
                ws.cell(row=17,column=1).value = inventarios.procesador.upper()
                ws.cell(row=17,column=3).value = inventarios.ram.upper()
                ws.cell(row=17,column=4).value = inventarios.sistema.upper()
                ws.cell(row=17,column=6).value = inventarios.disco.upper()
                ws.cell(row=17,column=7).value = inventarios.placa.upper()
                ws.cell(row=17,column=9).value = inventarios.office.upper()

            for extras in persona.extras.all():
                ws['A19'] = 'ADICIONALES INCLUIDOS'
                if extras.name=='Maletin':
                    ws['A20'] = '-MALETIN'
                else:
                    if extras.name == 'Mouse':
                        ws['A21'] = '-MOUSE'
                    else:
                        if extras.name == 'Cargador':
                            ws['A22'] = '-CARGADOR'
                        else:
                            if extras.name == 'Candado':
                                ws['A23'] = '-CANDADO'
                            else:
                                if extras.name == 'Cable de red':
                                    ws['A24'] = '-CABLE DE RED'
        #Establecemos el nombre del archivo
        nombre_archivo ="FICHA ENTREGA EQUIPAMIENTO.xlsx"
        #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
        response = HttpResponse(content_type="application/ms-excel") 
        contenido = "attachment; filename={0}".format(nombre_archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)
        return response


class AdministradorSignUpView(CreateView):
    model = User
    form_class = AdministradorSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'Administrador'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('administrador:index')

@login_required
@administrador_required
def index(request):
    inventario = Inventario.objects.all()
    return render(request, "inventario_app/administrador/index.html", {'inventario':inventario})

@login_required
@administrador_required
def crear_inventario(request):
    ImageFormset = modelformset_factory(Images, fields=('image',))

    if request.method == 'POST':
        form = CrearInventarioForm(request.POST,  request.FILES)
        formset = ImageFormset(request.POST or None, request.FILES or None)
        if form.is_valid() and formset.is_valid():
            inventario = form.save(commit=False)
            inventario.user = request.user
            inventario.save()
            form.save_m2m()            
            print(formset.cleaned_data)
            for f in formset:
                print(f.cleaned_data)
                try:
                    image = Images(inventario=inventario, image=f.cleaned_data.get('image'))
                    image.save()
                except Exception as e:
                    break
            messages.success(request, 'Inventario agregado')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = CrearInventarioForm()
        formset = ImageFormset(queryset=Images.objects.none())

    return render(request, 'inventario_app/administrador/inventario/crearinventario.html',{'form':form,'formset':formset})


@login_required
@administrador_required
def change_inventario(request, pk):

    inventario = get_object_or_404(Inventario, pk=pk)
    ImageFormset = modelformset_factory(Images, fields=('image',))
    if request.method == 'POST':
        form = EditarInventarioForm(request.POST,  request.FILES, instance=inventario)
        formset = ImageFormset(request.POST, request.FILES)
        if form.is_valid() and formset.is_valid():
            with transaction.atomic():
                
                form.save()
            print(formset.cleaned_data)
            data = Images.objects.filter(inventario=inventario)
            for index, f in enumerate(formset):
                if f.cleaned_data:
                    if f.cleaned_data['id'] is None:
                        photo = Images(inventario=inventario, image=f.cleaned_data.get('image'))
                        photo.save()
                    else:
                        photo = Images(inventario=inventario, image=f.cleaned_data.get('image'))
                        d = Images.objects.get(pk=data[index].id)
                        d.image = photo.image
                        d.save()                
            messages.success(request, 'Inventario editado')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = EditarInventarioForm(instance=inventario)
        formset = ImageFormset(queryset=Images.objects.filter(inventario=inventario))

    return render(request, 'inventario_app/administrador/inventario/editar_inventario.html',{'inventario':inventario,'form':form,'formset':formset})

@method_decorator([login_required, administrador_required], name='dispatch')
class InventarioDeleteView(DeleteView):
    model = Inventario
    template_name = 'inventario_app/administrador/inventario/eliminar_inventario.html'
    success_url = reverse_lazy('administrador:index')

    def delete(self, request, *args, **kwargs):
        inventario = self.get_object()
        messages.success(request, 'El inventario ah sido borrado')
        return super().delete(request, *args, **kwargs)




@login_required
@administrador_required
def crear_adicional(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = CrearCaracteristicasForm(request.POST,  request.FILES)
        if form.is_valid():
            adicional = form.save(commit=False)
            adicional.inventario = inventario
            adicional.save()
            messages.success(request, 'Adicional creado')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = CrearCaracteristicasForm()
    return render(request, 'inventario_app/administrador/inventario/crear_adicional.html',{'inventario':inventario,'form':form})

@login_required
@administrador_required
def adicional_change(request, inventario_pk, adicional_pk):

    inventario = get_object_or_404(Inventario, pk=inventario_pk)
    adicional = get_object_or_404(Adicional, pk=adicional_pk, inventario=inventario)

    if request.method == 'POST':
        form = CrearCaracteristicasForm(request.POST,  request.FILES, instance=adicional)
        if form.is_valid():
            with transaction.atomic():
                form.save()
            messages.success(request, 'Adicional editado')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = CrearCaracteristicasForm(instance=adicional)
    return render(request, 'inventario_app/administrador/inventario/editar_adicional.html',{'inventario':inventario,'adicional':adicional,'form':form})



@login_required
@administrador_required
def crear_monitor(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = CrearMonitorForm(request.POST,  request.FILES)
        if form.is_valid():
            monitor = form.save(commit=False)
            monitor.inventario = inventario
            monitor.save()
            messages.success(request, 'Monitor creado')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = CrearMonitorForm()
    return render(request, 'inventario_app/administrador/inventario/crear_monitor.html',{'inventario':inventario,'form':form})


@login_required
@administrador_required
def monitor_change(request, inventario_pk, monitor_pk):

    inventario = get_object_or_404(Inventario, pk=inventario_pk)
    monitor = get_object_or_404(Monitor, pk=monitor_pk, inventario=inventario)

    if request.method == 'POST':
        form = CrearMonitorForm(request.POST,  request.FILES, instance=monitor)
        if form.is_valid():
            with transaction.atomic():
                form.save()
            messages.success(request, 'Monitor editado')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = CrearMonitorForm(instance=monitor)
    return render(request, 'inventario_app/administrador/inventario/editar_monitor.html',{'inventario':inventario,'monitor':monitor,'form':form})

@method_decorator([login_required, administrador_required], name='dispatch')
class MonitorDeleteView(DeleteView):
    model = Monitor
    context_object_name = 'monitor'
    template_name = 'inventario_app/administrador/inventario/eliminar_monitor.html'
    pk_url_kwarg = 'monitor_pk'

    def get_context_data(self, **kwargs):
        monitor = self.get_object()
        kwargs['inventario'] = monitor.inventario
        return super().get_context_data(**kwargs)


    def get_queryset(self):
        return Monitor.objects.all()

    def get_success_url(self):
        monitor = self.get_object()
        return reverse('administrador:inventario_detalle', kwargs={'pk': monitor.inventario_id})




@login_required
@administrador_required
def crear_incidencia(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = IncidenciaForm(request.POST,  request.FILES)
        if form.is_valid():
            incidencia = form.save(commit=False)
            incidencia.inventario = inventario
            incidencia.save()
            messages.success(request, 'Incidencia creada')
            return redirect('administrador:inventario_detalle', inventario.pk)
    else:
        form = IncidenciaForm()
    return render(request, 'inventario_app/administrador/inventario/crear_incidencia.html',{'inventario':inventario,'form':form})

@login_required
@administrador_required
def crear_reporte(request):

    if request.method == 'POST':
        form = ReporteForm(request.POST,  request.FILES)
        if form.is_valid():
            reporte = form.save(commit=False)
            reporte.user = request.user
            reporte.save()
            messages.success(request, 'Reporte creado')
            return redirect('administrador:reportes')
    else:
        form = ReporteForm()
    return render(request, 'inventario_app/administrador/reporte/crear_reporte.html',{'form':form})

@login_required
@administrador_required
def crear_despacho(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = DespachoForm(request.POST,  request.FILES)

        if form.is_valid():
            despacho = form.save(commit=False)
            despacho.user = request.user
            despacho.equipo = inventario
            despacho.origen = inventario.colegio
            inventario.colegio = despacho.destino
            inventario.save()
            despacho.save()
            form.save_m2m()
            messages.success(request, 'Despacho creado')
            return redirect('administrador:despacho_list')
    else:
        form = DespachoForm()
    return render(request, 'inventario_app/administrador/despacho/crear_despacho.html',{'inventario':inventario,'form':form})





@login_required
@administrador_required
def crear_reporte_new(request, pk):
    reporte = get_object_or_404(Reporte, pk=pk)
    if request.method == 'POST':
        form = ReporteNewForm(request.POST,  request.FILES)
        if form.is_valid():
            reportenew = form.save(commit=False)
            reportenew.reporte = reporte
            reportenew.user = request.user
            reportenew.save()
            messages.success(request, 'Reporte creado')
            return redirect('administrador:reportes_view', reporte.pk)
    else:
        form = ReporteNewForm()
    return render(request, 'inventario_app/administrador/reporte/crear_reporte_new.html',{ 'reporte':reporte, 'form':form})

@login_required
@administrador_required
def crear_insumo(request):

    if request.method == 'POST':
        form = InsumoForm(request.POST,  request.FILES)
        if form.is_valid():
            insumo = form.save(commit=False)
            insumo.user = request.user

            insumo.save()
            messages.success(request, 'Insumo Agregado')
            return redirect('administrador:insumo_index')
    else:
        form = InsumoForm()
    return render(request, 'inventario_app/administrador/insumo/insumo_form.html',{'form':form})

@login_required
@administrador_required
def insumo_list(request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse,colegio__id=1).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga,colegio__id=1).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=1).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=1).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=1).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/insumo_list.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def baluarte (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=8).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=8).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=8).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=8).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=8).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/baluarte.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def bc_maipu (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=3).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=3).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=3).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=3).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=3).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/bc_maipu.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def bc_farfana (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=2).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=2).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=2).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=2).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=2).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/la_farfana.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })


@login_required
@administrador_required
def el_bosque (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=4).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=4).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=4).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=4).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=4).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/el_bosque.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })


@login_required
@administrador_required
def inmaculada_vitacura (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=5).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=5).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=5).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=5).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=5).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/inmaculada_vitacura.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def los_alpes (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=6).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=6).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=6).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=6).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=6).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/los_alpes.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def manantial (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=7).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=7).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=7).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=7).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=7).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/manantial.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def sebastian_alcano (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=9).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=9).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=9).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=9).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=9).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/seastian_alcano.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def san_francisco (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=10).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=10).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=10).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=10).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=10).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/san_francisco.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def san_marcos (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=11).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=11).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=11).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=11).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=11).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/san_marcos.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def iom (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=12).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=12).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=12).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=12).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=12).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/iom.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def san_sebastia (request):
    mouse = 1
    cable_vga = 2
    cable_red = 3
    teclado = 4
    cable_hdmi = 5

    insumo = Insumoform.objects.filter(insumo__id=mouse)
    conteo = Insumoform.objects.filter(insumo__id=mouse, colegio__id=13).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_vga = Insumoform.objects.filter(insumo__id=cable_vga, colegio__id=13).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_red = Insumoform.objects.filter(insumo__id=cable_red, colegio__id=13).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_hdmi = Insumoform.objects.filter(insumo__id=cable_hdmi, colegio__id=13).values('cantidad').aggregate(suma=Sum('cantidad'))
    conteo_teclado = Insumoform.objects.filter(insumo__id=teclado, colegio__id=13).values('cantidad').aggregate(suma=Sum('cantidad'))

    return render(request, 'inventario_app/administrador/insumo/san_sebastia.html',{
        'insumo':insumo, 
        'conteo':conteo,
        'conteo_vga':conteo_vga,
        'conteo_red':conteo_red,
        'conteo_hdmi':conteo_hdmi,
        'conteo_teclado':conteo_teclado,
        })

@login_required
@administrador_required
def insumo_index(request):
    return render(request, 'inventario_app/administrador/insumo/insumo_index.html')


@login_required
@administrador_required
def descripcion(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)
    adicional = Adicional.objects.filter(inventario=inventario)
    monitor = Monitor.objects.filter(inventario=inventario)
    incidencia = Incidencia.objects.filter(inventario=inventario)
    return render(request, "inventario_app/administrador/inventario/descripcion.html", {'inventario':inventario, 'adicional':adicional,'monitor':monitor,'incidencia':incidencia })

@login_required
@administrador_required
def despacho_detail(request, pk):
    despacho = get_object_or_404(Despacho, pk=pk)
    

    return render (request, "inventario_app/administrador/despacho/despacho_detail.html", {"despacho":despacho})

@login_required
@administrador_required
def actividad(request):

    actividad = Inventario.objects.all()
    actividad_2 = Reporte.objects.all()
    return render(request, "inventario_app/administrador/actividad.html", {'actividad':actividad, 'actividad_2':actividad_2})

@login_required
@administrador_required
def reportes(request):
    reporte = Reporte.objects.all()
    return render(request, "inventario_app/administrador/reporte/reportes.html", {'reporte':reporte})


@login_required
@administrador_required
def reportes_view(request, pk):
    reporte = get_object_or_404(Reporte, pk=pk)
    reporte_new = ReporteNew.objects.filter(reporte=reporte).order_by('-tiempo')

    return render(request, "inventario_app/administrador/reporte/reporte_view.html", {'reporte':reporte, 'reporte_new':reporte_new})

@login_required
@administrador_required
def usuarios(request):

    usuarios = User.objects.filter(is_superuser=False)

    return render(request, "inventario_app/administrador/usuarios.html", {'usuarios':usuarios})


@login_required
@administrador_required
def despacho(request):
    despacho = Despacho.objects.all()
    return render(request, "inventario_app/administrador/despacho/despacho_list.html",{'despacho':despacho})
