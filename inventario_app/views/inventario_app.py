import json
import traceback
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.views.generic import (TemplateView,CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from inventario_app.models import Inventario, Marca, User, Equipo, Administrador, Colegios
# Create your views here.

class SignUpView(TemplateView):
    template_name = 'registration/signup.html'


def home(request):
    if request.user.is_authenticated:
        if request.user.is_administrador:
            return redirect('administrador:index')
        if request.user.is_soporte:
            return redirect('soporte:index')
        if request.user.is_laboratorio:
            return redirect('laboratorio:index')
    return render(request, 'inventario_app/home.html')


